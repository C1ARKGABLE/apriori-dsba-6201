# Apriori Algorithm in Python

## This repo is a sample of what the Apriori Algorithm looks like using Python3.8 and Pandas1.0

## Problem set given:
### Question 1: 
Use Apriori Tid algorithm to find all the frequent item sets with Minimum Support = 4

|    | Pattern    |   Support |   Confidence |
|---:|:-----------|----------:|-------------:|
|  0 | (1.0, 2.0) |         4 |     0.8      |
|  7 | (2.0, 4.0) |         4 |     0.5      |
|  8 | (2.0, 5.0) |         5 |     0.625    |
| 19 | (5.0, 7.0) |         5 |     0.714286 |


### Question 2: 
Use the Apriori Algorithm to find frequent item sets and sample association rules with Minimum Support = 3 and Minimum Confidence 20%
|    | Pattern    |   Support |   Confidence |
|---:|:-----------|----------:|-------------:|
|  0 | (1.0, 2.0) |         4 |     0.666667 |
|  1 | (1.0, 3.0) |         4 |     0.666667 |
|  4 | (2.0, 3.0) |         4 |     0.571429 |
|  5 | (2.0, 4.0) |         3 |     0.428571 |
|  6 | (2.0, 5.0) |         3 |     0.428571 |


