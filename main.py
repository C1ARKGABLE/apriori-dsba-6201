from itertools import combinations
import pandas as pd
import numpy as np

# Read in the datasets
q1_data = pd.read_csv("q1_data.csv", index_col=0)
q2_data = pd.read_csv("q2_data.csv", index_col=0)


def apriori(data, min_sup=2, min_len=2, min_conf=0, min_lift=0):
    """
    This function takes a raw dataset, with TID format, and returns the item sets that satisfy the requirements.

    Args:
        data (df): This is the dataset to take in
        min_sup (int, optional): This is the minimum support value to be returned. Defaults to 2.
        min_len (int, optional): This is the minimum length of item sets. Defaults to 2.
        min_conf (float, optional): This is the minimum confidence value to be returned. Defaults to 0.
        min_lift (float, optional): This is the minimum lift value to be returned. Defaults to 0.

    Returns:
        [df]: This is the returned dataframe that contains all item sets that meet the criteria
    """

    # This is some SQL style witchcraft...
    # First we unstack, then drop NaNs, then we get the dummies, then group them, then sum them to get counts
    df = pd.get_dummies(data.unstack().dropna()).groupby(level=1).sum()

    # Get the shape, as it will be handy later on
    col_len, row_len = df.shape

    # Initalize a list and dictionary for later
    pattern = []
    singletons = {}

    # Find all of the singletons and calculate their support. Store this in the dictionary for later use.
    for cols in combinations(df, 1):
        pattern_sup = df[list(cols)].all(axis=1).sum()
        singletons[cols[0]] = pattern_sup

    # For all non-singleton combinations, what's the support and confidence for that pattern
    for cnum in range(min_len, row_len + 1):
        for cols in combinations(df, cnum):
            pattern_conf = pattern_sup / singletons[cols[0]]

            pattern_lift = pattern_sup / (singletons[cols[0]] * singletons[cols[1]])

            pattern.append([str(cols), pattern_sup, pattern_conf, pattern_lift])

    # Put it into a pretty named dataframe for ease of use
    sdf = pd.DataFrame(pattern, columns=["Pattern", "Support", "Confidence", "Lift"])
    # Filter based on criteria
    print(sdf.to_markdown())
    results = sdf[
        (sdf.Support >= min_sup) & (sdf.Confidence >= min_conf) & (sdf.Lift >= min_lift)
    ]
    # return the results
    return results


# Get and print the answer using dataset 1
answer_1 = apriori(q1_data, min_sup=4)
print(answer_1.to_markdown())

# Get and print the answer using dataset 2
answer_2 = apriori(q2_data, min_sup=3, min_conf=0.2)
print(answer_2.to_markdown())

